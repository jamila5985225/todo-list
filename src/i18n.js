import { createI18n } from 'vue-i18n'
import en from './traduction/en.json'
import fr from './traduction/fr.json'

function loadLocaleMessages() {
  const traduction = [{ en: en }, { fr: fr }]
  const messages = {}
  traduction.forEach(lang => {
    const key = Object.keys(lang)
    messages[key] = lang[key] 
  })
  return messages
}

export default createI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: loadLocaleMessages()
})