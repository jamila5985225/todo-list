// store/index.js
import { createStore } from 'vuex';

const store = createStore({
  state: {
    todos: [],
    archivedTodos: []
  },
  mutations: {
    addTodo(state, todo) {
      state.todos.push(todo);
    },
    removeTodo(state, todo) {
      let indexOft = state.todos.indexOf(todo);
      state.todos.splice(indexOft, 1);
    },
    toggleTodoStatus(state, index) {
      state.todos[index].completed = !state.todos[index].completed;
    },
    archiveTodo(state, index) {
      const todoToArchive = state.todos[index];
      state.todos.splice(index, 1); 
      state.archivedTodos.push(todoToArchive); 
    }
  },
  actions: {
    addTodoAction({ commit }, todo) {
      commit('addTodo', todo);
    },
    removeTodoAction({ commit }, todo) {
      commit('removeTodo', todo);
    },
    toggleTodoStatusAction({ commit }, index) {
      commit('toggleTodoStatus', index);
    },
    archiveTodoAction({ commit }, index) {
      commit('archiveTodo', index);
    }
  },
  getters: {
    allTodos: state => state.todos,
    // completedTodos: state => state.todos.filter(todo => todo.completed),
    // incompleteTodos: state => state.todos.filter(todo => !todo.completed)
  }
});

export default store;
