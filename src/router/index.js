import { createRouter, createWebHistory } from 'vue-router'
import todolist from '@/components/todoList.vue';
import archive from '@/components/archive.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'todoList',
      component: todolist
    },
    {
      path: '/archive',
      name: 'archive',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: archive
    }
  ]
})

export default router
